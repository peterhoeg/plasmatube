msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-27 00:48+0000\n"
"PO-Revision-Date: 2023-05-22 14:01\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/plasmatube/plasmatube.pot\n"
"X-Crowdin-File-ID: 25498\n"

#: contents/ui/ChannelPage.qml:82 contents/ui/SearchPage.qml:180
#: contents/ui/TrendingPage.qml:123
#, kde-format
msgctxt "@info:status"
msgid "Loading…"
msgstr ""

#: contents/ui/components/BottomNavBar.qml:69
#: contents/ui/components/Sidebar.qml:60
#, kde-format
msgid "Videos"
msgstr "视频"

#: contents/ui/components/BottomNavBar.qml:81
#: contents/ui/components/Sidebar.qml:73 contents/ui/SearchPage.qml:17
#: contents/ui/SearchPage.qml:174
#, kde-format
msgid "Search"
msgstr "搜索"

#: contents/ui/components/BottomNavBar.qml:92
#: contents/ui/components/Sidebar.qml:94 contents/ui/SettingsPage.qml:14
#: contents/ui/videoplayer/VideoControls.qml:239
#, kde-format
msgid "Settings"
msgstr "设置"

#: contents/ui/LoginPage.qml:13
#, kde-format
msgid "Sign in"
msgstr "登录"

#: contents/ui/LoginPage.qml:19
#, kde-format
msgid "Successfully logged in as %1."
msgstr "成功以 %1 的身份登录。"

#: contents/ui/LoginPage.qml:46
#, kde-format
msgid "Log in to an Invidious Account"
msgstr ""

#: contents/ui/LoginPage.qml:51
#, kde-format
msgid "Instance Url"
msgstr ""

#: contents/ui/LoginPage.qml:63
#, kde-format
msgid "Username"
msgstr "用户名"

#: contents/ui/LoginPage.qml:74
#, kde-format
msgid "Password"
msgstr ""

#: contents/ui/LoginPage.qml:86 contents/ui/SettingsPage.qml:63
#, kde-format
msgid "Log in"
msgstr ""

#: contents/ui/LoginPage.qml:93
#, kde-format
msgid "Instance URL must not be empty!"
msgstr ""

#: contents/ui/LoginPage.qml:98
#, kde-format
msgid "Username must not be empty!"
msgstr ""

#: contents/ui/LoginPage.qml:103
#, kde-format
msgid "Password must not be empty!"
msgstr ""

#: contents/ui/SearchPage.qml:70
#, kde-format
msgid "Sort By:"
msgstr "排序方式："

#: contents/ui/SearchPage.qml:74
#, kde-format
msgid "Rating"
msgstr "评分高低"

#: contents/ui/SearchPage.qml:85
#, kde-format
msgid "Relevance"
msgstr "相关程度"

#: contents/ui/SearchPage.qml:97
#, kde-format
msgid "Upload Date"
msgstr "上传日期"

#: contents/ui/SearchPage.qml:108
#, kde-format
msgid "View Count"
msgstr "观看次数"

#: contents/ui/SearchPage.qml:174
#, kde-format
msgid "No results"
msgstr "无结果"

#: contents/ui/SettingsPage.qml:25
#, kde-format
msgid "Invidious"
msgstr "偏好"

#: contents/ui/SettingsPage.qml:29
#, kde-format
msgid "Instance"
msgstr "实例"

#: contents/ui/SettingsPage.qml:44
#, kde-format
msgid "Account"
msgstr "账户"

#: contents/ui/SettingsPage.qml:50
#, kde-format
msgid "Currently logged in as %1."
msgstr "目前已经以 %1 身份登录。"

#: contents/ui/SettingsPage.qml:70
#, kde-format
msgid "Log out"
msgstr "注销"

#: contents/ui/SettingsPage.qml:88
#, kde-format
msgid "About PlasmaTube"
msgstr ""

#: contents/ui/VideoGridItem.qml:162
#, kde-format
msgid "• %1 views • %2"
msgstr "• %1 次观看 • %2"

#: contents/ui/VideoListItem.qml:157
#, kde-format
msgid " • %1 views"
msgstr " • %1 次观看"

#: contents/ui/VideoListItem.qml:165
#, kde-format
msgid " • %1"
msgstr " • %1"

#: contents/ui/videoplayer/VideoPlayer.qml:321
#, kde-format
msgid "Unsubscribe (%1)"
msgstr ""

#: contents/ui/videoplayer/VideoPlayer.qml:323
#, kde-format
msgid "Subscribe (%1)"
msgstr ""

#: contents/ui/videoplayer/VideoPlayer.qml:333
#, kde-format
msgid "Please log in to subscribe to channels."
msgstr ""

#: contents/ui/videoplayer/VideoPlayer.qml:354
#, kde-format
msgid "%1 views"
msgstr "%1 次观看"

#: contents/ui/videoplayer/VideoPlayer.qml:362
#, kde-format
msgid "%1 Likes"
msgstr "%1 个点赞"

#: logincontroller.cpp:46 qinvidious/invidiousapi.cpp:97
#, kde-format
msgid "Username or password is wrong."
msgstr ""

#: logincontroller.cpp:49
#, kde-format
msgid "This instance has disabled the registration."
msgstr ""

#: main.cpp:47
#, kde-format
msgid "PlasmaTube"
msgstr ""

#: main.cpp:49 main.cpp:91
#, kde-format
msgid "YouTube client"
msgstr ""

#: main.cpp:51
#, kde-format
msgid "© Linus Jahn"
msgstr ""

#: main.cpp:52
#, kde-format
msgid "Linus Jahn"
msgstr ""

#: main.cpp:52
#, kde-format
msgid "Creator"
msgstr ""

#: main.cpp:53
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#: main.cpp:53
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""

#: qinvidious/invidiousapi.cpp:187
#, kde-format
msgid "Server returned no valid JSON."
msgstr ""

#: qinvidious/invidiousapi.cpp:196
#, kde-format
msgid "Server returned the status code %1"
msgstr ""

#: videolistmodel.cpp:17
#, kde-format
msgid "Subscriptions"
msgstr ""

#: videolistmodel.cpp:19
#, kde-format
msgid "Invidious Top"
msgstr ""

#: videolistmodel.cpp:21
#, kde-format
msgid "Trending"
msgstr ""

#: videolistmodel.cpp:23
#, kde-format
msgid "Trending Gaming"
msgstr ""

#: videolistmodel.cpp:25
#, kde-format
msgid "Trending Movies"
msgstr ""

#: videolistmodel.cpp:27
#, kde-format
msgid "Trending Music"
msgstr ""

#: videolistmodel.cpp:29
#, kde-format
msgid "Trending News"
msgstr ""

#: videolistmodel.cpp:182
#, kde-format
msgid "Search results for \"%1\""
msgstr ""
